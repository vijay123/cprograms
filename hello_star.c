#include <stdio.h>
// we are going to use printf function in our program

int main() { // entry point of program
	printf("**************\n");
	printf(" **********\n");	 // to show string on console output screen (black screen)
	printf("  ********\n");
	printf("   *****\n");
	printf("    ***\n");
	printf("     *\n");
	getch();
}
